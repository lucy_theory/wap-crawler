/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap.dao;

import com.xcom.crawler.wap.model.Lottery;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import vn.wedriver.common.bonecp.ConnectionManager;

/**
 *
 * @author xcom
 */
public class WapCrawlerDao {

  private String database = "main";
  private String sqlLastResult = "select * from (select * from lottery_result order by report_date desc) where rownum < 2";
  private final String sqlInsert = "insert into lottery_result values (lottery_result_seq.nextval,?,?,?,?,?,?,?,?,?,1,?,0,?,?)";
  private final String sqlUpdate = "update lottery_result set result = ?,report_date = ?, num1=?,num2=?,num3=?,num4=?,num5=?,num6=?,num7=?,is_active=?,updated_at = ? where id = ?";

  public void setDatabase(String database) {
    this.database = database;
  }

  public void setSqlLastResult(String sqlLastResult) {
    this.sqlLastResult = sqlLastResult;
  }

  public Lottery findLastResult() throws Exception {
    Lottery lt;
    try (Connection conn = ConnectionManager.getInstance().getConnection(database);
            PreparedStatement pst = conn.prepareStatement(sqlLastResult);
            ResultSet rs = pst.executeQuery();) {
      lt = new Lottery();
      while (rs.next()) {
        lt.setId(rs.getLong("ID"));
        lt.setResult(rs.getString("RESULT"));
        lt.setReportDate(rs.getTimestamp("REPORT_DATE") != null ? new Date(rs.getTimestamp("REPORT_DATE").getTime()) : null);
        lt.setNum1(rs.getInt("NUM1"));
        lt.setNum2(rs.getInt("NUM2"));
        lt.setNum3(rs.getInt("NUM3"));
        lt.setNum4(rs.getInt("NUM4"));
        lt.setNum5(rs.getInt("NUM5"));
        lt.setNum6(rs.getInt("NUM6"));
        lt.setNum7(rs.getInt("NUM7"));
        lt.setStatus(rs.getInt("STATUS"));
        break;
      }
    }
    return lt;
  }

  public int update(Lottery lottery) throws Exception {
    int rs;
    try (Connection conn = ConnectionManager.getInstance().getConnection(database);
            PreparedStatement pst = buildUpdated(conn, lottery);) {
      rs = pst.executeUpdate();
    }
    return rs;
  }

  private PreparedStatement buildUpdated(Connection conn, Lottery lottery) throws SQLException {
    PreparedStatement pst = conn.prepareStatement(sqlUpdate);
    pst.setString(1, lottery.buildResult());
    pst.setTimestamp(2, new Timestamp(lottery.getReportDate().getTime()));
    pst.setInt(3, lottery.getNum1());
    pst.setInt(4, lottery.getNum2());
    pst.setInt(5, lottery.getNum3());
    pst.setInt(6, lottery.getNum4());
    pst.setInt(7, lottery.getNum5());
    pst.setInt(8, lottery.getNum6());
    pst.setInt(9, lottery.getNum7());
    pst.setInt(10, lottery.getIsActive());
    pst.setTimestamp(11, new Timestamp(new Date().getTime()));
    pst.setLong(12, lottery.getId());
    return pst;
  }

  public int insert(Lottery lottery) throws Exception {
    int rs;
    try (Connection conn = ConnectionManager.getInstance().getConnection(database);
            PreparedStatement pst = buildInsert(conn, lottery);) {
      rs = pst.executeUpdate();
    }
    return rs;
  }

  private PreparedStatement buildInsert(Connection conn, Lottery lottery) throws SQLException {
    PreparedStatement pst = conn.prepareStatement(sqlInsert);
    pst.setString(1, lottery.buildResult());
    pst.setTimestamp(2, new Timestamp(lottery.getReportDate().getTime()));
    pst.setInt(3, lottery.getNum1());
    pst.setInt(4, lottery.getNum2());
    pst.setInt(5, lottery.getNum3());
    pst.setInt(6, lottery.getNum4());
    pst.setInt(7, lottery.getNum5());
    pst.setInt(8, lottery.getNum6());
    pst.setInt(9, lottery.getNum7());
    pst.setInt(10, lottery.getIsActive());
    pst.setTimestamp(11, new Timestamp(new Date().getTime()));
    pst.setTimestamp(12, new Timestamp(new Date().getTime()));
    return pst;
  }

}
