/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap.model;

import java.util.Date;

/**
 *
 * @author xcom
 */
public class Lottery {

  private long id;
  private String result;
  private Date reportDate;
  private int num1 = -1;
  private int num2 = -1;
  private int num3 = -1;
  private int num4 = -1;
  private int num5 = -1;
  private int num6 = -1;
  private int num7 = -1;
  private boolean isMorning;
  private int status;
  private Date createddAt;
  private Date updatedAt;

  public Lottery() {
    reportDate = new Date();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public Date getReportDate() {
    return reportDate;
  }

  public void setReportDate(Date reportDate) {
    this.reportDate = reportDate;
  }

  public int getNum1() {
    return num1;
  }

  public void setNum1(int num1) {
    this.num1 = num1;
  }

  public int getNum2() {
    return num2;
  }

  public void setNum2(int num2) {
    this.num2 = num2;
  }

  public int getNum3() {
    return num3;
  }

  public void setNum3(int num3) {
    this.num3 = num3;
  }

  public int getNum4() {
    return num4;
  }

  public void setNum4(int num4) {
    this.num4 = num4;
  }

  public int getNum5() {
    return num5;
  }

  public void setNum5(int num5) {
    this.num5 = num5;
  }

  public int getNum6() {
    return num6;
  }

  public void setNum6(int num6) {
    this.num6 = num6;
  }

  public int getNum7() {
    return num7;
  }

  public void setNum7(int num7) {
    this.num7 = num7;
  }

  public boolean isIsMorning() {
    return isMorning;
  }

  public void setIsMorning(boolean isMorning) {
    this.isMorning = isMorning;
  }

  public int getIsActive() {
    if (isFull()) {
      return 1;
    } else {
      return 0;
    }
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Date getCreateddAt() {
    return createddAt;
  }

  public void setCreateddAt(Date createddAt) {
    this.createddAt = createddAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }


  /**
   * Trang thai da duoc update day du
   *
   * @return
   */
  public boolean isFull() {
    return num1 > -1 && num2 > -1 && num3 > -1 && num4 > -1 && num5 > -1 && num6 > -1 && num7 > -1;
  }

  public String buildResult() {
    StringBuilder str = new StringBuilder();
    if (num1 > -1) {
      str.append(num1);
    }
    if (num2 > -1) {
      str.append(num2);
    }
    if (num3 > -1) {
      str.append(num3);
    }
    if (num4 > -1) {
      str.append(num4);
    }
    if (num5 > -1) {
      str.append(num5);
    }
    if (num6 > -1) {
      str.append(num6);
    }
    if (num7 > -1) {
      str.append(num7);
    }
    result = str.toString();
    return result;
  }
}
