/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap.model;

import java.util.Date;

/**
 * $('#game dl .resultsRow [class="sprite-results"]').text() <br/>
 * $('#game dl .resultsNextDrawInfo p:first').text()
 *
 * @author xcom
 */
public class Numbers {

  private Date reportDate;
  private int num1 = -1;
  private int num2 = -1;
  private int num3 = -1;

  public Date getReportDate() {
    return reportDate;
  }

  public void setReportDate(Date reportDate) {
    this.reportDate = reportDate;
  }

  public int getNum1() {
    return num1;
  }

  public void setNum1(int num1) {
    this.num1 = num1;
  }

  public int getNum2() {
    return num2;
  }

  public void setNum2(int num2) {
    this.num2 = num2;
  }

  public int getNum3() {
    return num3;
  }

  public void setNum3(int num3) {
    this.num3 = num3;
  }

  @Override
  public String toString() {
    return num1 + "|" + num2 + "|" + num3 + "|" + reportDate;
  }

}
