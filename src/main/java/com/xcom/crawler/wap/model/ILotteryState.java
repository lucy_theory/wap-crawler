/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap.model;

/**
 *
 * @author xcom
 */
public interface ILotteryState {

  public final int OUT_OF_DATE = 1;//lottery da qua cu, can duoc cap nhat
  public final int UPDATING = 2;//trang thai dang trong duration cap nhat
  public final int UPDATED = 3;//da duoc cap nhat hoan thanh

}
