/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap;

import com.xcom.crawler.wap.crontab.CrawlerJob;
import com.xcom.crawler.wap.dao.WapCrawlerDao;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import vn.wedriver.common.bonecp.ConnectionManager;

/**
 *
 * @author xcom
 */
public class Crawler {

  private static Crawler instance;

  public static Crawler getInstance() throws Exception {
    synchronized (Crawler.class) {
      if (null == instance) {
        instance = new Crawler();
      }
      return instance;
    }
  }
  private final Logger logger = Logger.getLogger(Crawler.class);
  private final CoreBusiness coreBusiness;
  private final Config config;
  private final Scheduler scheduler;
  private WapCrawlerControl crawler;

  public Crawler() throws Exception {
    PropertyConfigurator.configure("../etc/log4j.properties");
    config = new Config();
    ConnectionManager.getInstance().load();
    WapCrawlerDao lotteryDao = new WapCrawlerDao();
    coreBusiness = new CoreBusiness(config, lotteryDao);
    logger.info("initialization corontab");
    SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    scheduler = schedFact.getScheduler();
    //khoi tao bien crawler cho tung trang
    crawler = new WapCrawlerControl(config,
            config.get("system_urlnumber_evening", String.class, "https://www.lotterypost.com/game/146"),
            config.get("system_evening_folder", String.class, "../data/crawl/evening"));
    addCrontab(CrawlerJob.class, "evening-crawler", config.get("system_trigger_evening", String.class, "0/15 48 9 * * ?"), "wap_crawler", crawler);
  }

  public CoreBusiness getCoreBusiness() {
    return coreBusiness;
  }

  public void start() throws Exception {
    scheduler.start();
    logger.info("START APP!!!");
  }

  public void stop() throws Exception {
    scheduler.standby();
    logger.info("STOP APP!!!");
  }

  private void addCrontab(Class aClass, String name, String triggerTime, String dataName, Object data) throws SchedulerException {
    //khoi tao crontab 
    JobDetail crontab = newJob(aClass).withIdentity("crawler", name).build();
    crontab.getJobDataMap().put(dataName, data);
    Trigger trigger = newTrigger().startNow().withSchedule(cronSchedule(triggerTime)).build();
    scheduler.scheduleJob(crontab, trigger);
  }

}
