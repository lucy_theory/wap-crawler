/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap;

import com.xcom.crawler.wap.dao.WapCrawlerDao;
import com.xcom.crawler.wap.model.ILotteryState;
import com.xcom.crawler.wap.model.Lottery;
import com.xcom.crawler.wap.model.Numbers;
import com.xcom.crawler.wap.model.Win4;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author xcom
 */
public class CoreBusiness {

  private final Logger logger = Logger.getLogger(CoreBusiness.class);
  private final WapCrawlerDao crawlerDao;
  private Lottery lottery;
  private long duration = 3600 * 1000;//trong khoang 1 gio

  public CoreBusiness(Config config, WapCrawlerDao crawlerDao) {
    this.crawlerDao = crawlerDao;
    this.duration = 1000 * config.get("system_duration", Integer.class, 3600);
  }

  public void loadLastLottery() throws Exception {
    lottery = crawlerDao.findLastResult();
  }

  public void updateNumber(Numbers number) throws Exception {
    int state = checkSate(number.getReportDate());
    switch (state) {
      case ILotteryState.OUT_OF_DATE:
        lottery = new Lottery();
      case ILotteryState.UPDATING:
        logger.info("NUMBER: " + number);
        logger.info("number is new -> update lottery ");
        lottery.setNum1(number.getNum1());
        lottery.setNum2(number.getNum2());
        lottery.setNum3(number.getNum3());
        lottery.setReportDate(number.getReportDate());
        updateLottery();
        break;
      case ILotteryState.UPDATED:
        logger.info("Lottery (" + lottery.getResult() + "|" + lottery.getReportDate() + ") is updated");
        break;
      default:
        logger.info("State invalid");
    }
  }

  public void updateWin4(Win4 number) throws Exception {
    int state = checkSate(number.getReportDate());
    switch (state) {
      case ILotteryState.OUT_OF_DATE:
        lottery = new Lottery();
      case ILotteryState.UPDATING:
        logger.info("WIN4: " + number);
        logger.info("Win4 is new -> update lottery ");
        lottery.setNum4(number.getNum1());
        lottery.setNum5(number.getNum2());
        lottery.setNum6(number.getNum3());
        lottery.setNum7(number.getNum4());
        lottery.setReportDate(number.getReportDate());
        updateLottery();
        break;
      case ILotteryState.UPDATED:
        logger.info("Lottery (" + lottery.getResult() + "|" + lottery.getReportDate() + ") is updated");
        break;
      default:
        logger.info("State invalid");
    }
    //ket qua chua duoc cap nhat day du va thoi gian cap nhat trong khoang cho phep -> tiep tuc cap nhat
    if (lottery.getId() < 1 || (!lottery.isFull() && Math.abs(lottery.getReportDate().getTime() - number.getReportDate().getTime()) < duration)) {

    }
  }

  private void updateLottery() throws Exception {
    if (lottery.getId() > 0) {
      crawlerDao.update(lottery);
      logger.info("update lottery|" + lottery.buildResult());
    } else {
      crawlerDao.insert(lottery);
      logger.info("insert lottery|" + lottery.buildResult());
    }
  }

  private int checkSate(Date reportDate) {
    if (null == lottery) {
      return ILotteryState.OUT_OF_DATE;
    }
    if (reportDate.getTime() - lottery.getReportDate().getTime() > duration) {
      return ILotteryState.OUT_OF_DATE;
    }
    if ((reportDate.getTime() - lottery.getReportDate().getTime()) < duration && lottery.isFull()) {
      return ILotteryState.UPDATED;
    }
    return ILotteryState.UPDATING;
  }

}
