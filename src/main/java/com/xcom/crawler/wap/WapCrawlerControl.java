/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap;

import com.xcom.crawler.filter.WapParser;
import com.xcom.crawler.filter.Win4Crawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.apache.log4j.Logger;

/**
 *
 * @author xcom
 */
public class WapCrawlerControl {

    private final Logger logger = Logger.getLogger(WapCrawlerControl.class);
    private final String url;
    private String storageFolder = "../data/crawl/root";

    public WapCrawlerControl(Config config, String url, String storageFolder) {
        this.storageFolder = storageFolder;
        this.url = url;
    }

    public synchronized void crawl() throws Exception {
        String crawlStorageFolder = storageFolder;
        int numberOfCrawlers = 1;
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(crawlStorageFolder);
        config.setMaxDepthOfCrawling(0);
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);
        controller.addSeed(url);
        controller.start(WapParser.class, numberOfCrawlers);
    }
}
