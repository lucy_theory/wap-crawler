/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.wap.crontab;

import com.xcom.crawler.wap.WapCrawlerControl;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author xcom
 */
public class CrawlerJob implements Job {

  private static final Logger logger = Logger.getLogger(CrawlerJob.class);
  private boolean running = false;

  @Override
  public void execute(JobExecutionContext jec) throws JobExecutionException {
    synchronized (CrawlerJob.class) {
      if (running) {
        logger.info("Crawler is running -> skip");
        return;
      }
      running = true;
      logger.info("start Crawler job");
      JobDataMap data = jec.getJobDetail().getJobDataMap();
      try {
        WapCrawlerControl crawler = (WapCrawlerControl) data.get("wap_crawler");
        crawler.crawl();
        logger.info("stop Crawler job");
      } catch (Exception ex) {
        logger.error("crontab Crawler error", ex);
      }
      running = false;
    }
  }
}
