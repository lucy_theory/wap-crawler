/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xcom.crawler.filter;

import com.xcom.crawler.wap.CoreBusiness;
import com.xcom.crawler.wap.Crawler;
import com.xcom.crawler.wap.model.Numbers;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author xcom
 */
public class WapParser extends WebCrawler {

    private final static Logger log = Logger.getLogger(WapParser.class);
    private final String mainPage = "https://www.lotterypost.com";
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(css|js|gif|jpg"
            + "|png|mp3|mp3|zip|gz))$");
    private final SimpleDateFormat sdf = new SimpleDateFormat("EEEEEEEEE, MMMMMMMM dd, yyyy, hh:mm aaa");

    /**
     * This method receives two parameters. The first parameter is the page in
     * which we have discovered this new url and the second parameter is the new
     * url. You should implement this function to specify whether the given url
     * should be crawled or not (based on your crawling logic). In this example,
     * we are instructing the crawler to ignore urls that have css, js, git, ...
     * extensions and to only accept urls that start with
     * "http://www.ics.uci.edu/". In this case, we didn't need the referringPage
     * parameter to make the decision.
     *
     * @param referringPage
     * @param url
     * @return
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILTERS.matcher(href).matches()
                && href.startsWith(mainPage);
    }

    /**
     * function xu ly code parser trang web o day
     * by your program.
     *
     * @param page
     */
    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();

        System.out.println("URL: " + url);
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String text = htmlParseData.getText();
            String html = htmlParseData.getHtml();
            Set<WebURL> links = htmlParseData.getOutgoingUrls();
            Document doc = Jsoup.parse(html);
            System.out.println("title " + doc.title());
            try {
                Numbers number = buildNumbers(doc);
                CoreBusiness business = Crawler.getInstance().getCoreBusiness();
                business.loadLastLottery();
                business.updateNumber(number);
            } catch (Exception ex) {
                log.error("parser number error", ex);
            }
        }
    }

    private Numbers buildNumbers(Document doc) throws ParseException {
        Numbers n = new Numbers();
        //ket qua cac so
        Elements els = doc.select("#game dl .resultsRow").first().select("ins");
        if (els != null && els.size() > 0) {
            n.setNum1(Integer.parseInt(els.get(0).text()));
        }
        if (els != null && els.size() > 1) {
            n.setNum2(Integer.parseInt(els.get(1).text()));
        }
        if (els != null && els.size() > 2) {
            n.setNum3(Integer.parseInt(els.get(2).text()));
        }
        Element date = doc.select("#game dl .resultsNextDrawInfo p").first();
        if (date != null) {
            Date d = sdf.parse(date.text());
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.DATE, -1);//before next drawing once day
            n.setReportDate(cal.getTime());
        }
        return n;
    }
}
